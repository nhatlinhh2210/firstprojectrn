import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  // Hien thi the cuon
  ScrollView,
  // Hien thi danh sach nhung kh can key property
  FlatList,
  // Item trong list co the bat su kien click
  TouchableOpacity,
} from "react-native";

export default function App() {
  //Tao bien name de chu du lieu, setName de goi ham chinh sua
  const [name, setName] = useState("****");
  const [age, setAge] = useState("**");

  //Tao bien person chua du lieu, co the co nhieu dang du lieu trong bien
  const [personAut, setPersonAut] = useState({ nameAut: "***", ageAut: "**" });

  // Tao mang du lieu, key de phan biet tung du lieu
  const [arrayName, setArrayName] = useState([
    { aName: "Gunner", key: "1" },
    { aName: "Blade Master", key: "2" },
    { aName: "Blade Dancer", key: "3" },
    { aName: "Archer", key: "4" },
    { aName: "Warlock", key: "5" },
    { aName: "Warden", key: "6" },
    { aName: "Kungfu Master", key: "7" },
    { aName: "Soul Fighter", key: "8" },
  ]);

  // Tao function click
  const clickHandler = () => {
    // goi function setName o tren de thay doi chu ben trong
    setName("Linh");
    setAge(20);
    setPersonAut({ nameAut: "iCy", ageAut: 20 });
  };

  // Function pressHandler
  const pressHandler = (key) => {
    // Gui ve console gia tri key cua item da click vao
    console.log(key);
  };

  return (
    // Khai bao view style container
    <View style={styles.container}>
      {/* khai bao the view header */}
      <View style={styles.header}>
        {/* Dung the boldText de lam kieu chu in dam */}
        <Text style={styles.boldText}>Hello World!</Text>
      </View>

      {/* khai bao body */}
      <View style={styles.body}>
        {/* Tao the Text Input */}
        <Text>Nhap ten:</Text>
        <TextInput
          // multiline de nhap nhieu dong
          multiline
          style={styles.input}
          // Goi y
          placeholder="Dien ten vao day"
          // Khi nhap vao se ghi vao setName thong qua gia tri val
          onChangeText={(val) => setName(val)}
        />

        <Text>Nhap tuoi:</Text>
        <TextInput
          // ban phim so
          keyboardType="numeric"
          style={styles.input}
          placeholder="Dien tuoi vao day"
          onChangeText={(val) => setAge(val)}
        />

        {/* Kieu boldText phai dung trong the Text thi moi hien thi duoc */}
        {/* Su dung bien {name} de goi ra bien da dat */}
        <Text style={styles.boldText}>La {name} day </Text>

        {/* Dung the boldText thi cac the <Text></Text> con deu la chu in dam */}
        <Text style={styles.boldText}>
          Con day la {name} <Text>{age} tuoi</Text>
        </Text>
      </View>

      {/* Khong dung the boldText thi kh co gi xay ra */}
      {/* Su dung bien {person.name}/{person.age} de goi ra du lieu can hien thi */}
      <Text>
        Made by {personAut.nameAut} {personAut.ageAut} years old.
      </Text>

      {/* Khai bao the Button */}
      <View style={styles.buttonContainer}>
        {/* ReactNative the Button dong bang /> */}
        {/* Dung the onPress de lay su kien click vao, khi click vao goi function clickHandler */}
        <Button title="DONE" onPress={clickHandler} />
      </View>

      {/* Dung the FlatList de tao list item */}
      <FlatList
        // So cot hien thi
        numColumns={2}
        // Khai bao key cua item
        keyExtractor={(item) => item.key}
        // Khai bao mang
        data={arrayName}
        // Xuat item
        renderItem={({ item }) => (
          // ham bat su kien click cho item, khi click vao item goi function pressHandler tra ve gia tri key
          <TouchableOpacity onPress={() => pressHandler(item.key)}>
            <Text style={styles.item}>{item.aName}</Text>
          </TouchableOpacity>
        )}
      />

      {/* Hien thi voi ScrollView  */}

      {/* <ScrollView> */}
      {/* Hien thi mang du lieu */}
      {/* <View style={styles.arrayContainer}> */}
      {/* Goi bien arrayName, su dung ham "map" de lay du lieu trong mang */}
      {/* tao the view item, voi "key" de phan biet tung item */}
      {/* {arrayName.map((item) => ( */}
      {/*  <View key={item.key}> */}
      {/* The text hien thi cac item co trong mang */}
      {/* <Text style={styles.item}>{item.aName}</Text> */}
      {/* </View> */}
      {/* ))} */}
      {/* </View> */}
      {/* </ScrollView> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  header: {
    backgroundColor: "#199",
    marginTop: 50,
    padding: 20,
  },
  boldText: {
    fontWeight: "bold",
  },
  body: {
    backgroundColor: "#159",
    padding: 20,
    //fontWeight: 'bold', khong the hien thi kieu bold
  },
  buttonContainer: {
    marginTop: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: "#000",
    padding: 8,
    margin: 10,
    width: 200,
  },
  item: {
    marginTop: 10,
    padding: 30,
    backgroundColor: "#199",
    color: "white",
    fontSize: 13,
    marginHorizontal: 10,
  },
});
